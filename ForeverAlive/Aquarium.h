#pragma once
#include "SFML\Graphics.hpp"
#include "BaseEntity.h"
class Aquarium
{
	friend BaseEntity;
private:
	Aquarium() { };
	static Aquarium* aquarium;
	void live();
	sf::RenderWindow renderWindow;
	std::vector<BaseEntity> inhabitants;
	Aquarium(Aquarium const&) = delete;
	Aquarium& operator= (Aquarium const&) = delete;
public:
	void setSettings(int predatorsCount, int herbivorousCount, int planktonsCount);
	void start();
};