#pragma once
#include "BaseEntity.h"
#include <typeinfo>
#include <string>

class BaseFish :
	public BaseEntity
{
protected:
	virtual void proliferation();
	virtual void move();
	std::string eatable;
};