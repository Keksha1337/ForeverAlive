#include "BaseEntity.h"
#include "Aquarium.h"

BaseEntity::BaseEntity() { }

bool BaseEntity::IsAlive() { return this->isAlive; }

void BaseEntity::live()
{
	this->move();
	this->draw();
	this->checkAlive();
}

void BaseEntity::draw()
{
	Aquarium::aquarium->renderWindow.draw(this->sprite);
}

void BaseEntity::checkAlive()
{
	if (eatCounter <= 0 && leftSecForDie <= 0)
		this->isAlive = false;
}