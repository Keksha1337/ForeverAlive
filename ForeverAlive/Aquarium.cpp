#include "Aquarium.h"
#include "PredatorFish.h"
#include "HerbivoresFish.h"
#include "Plankton.h"

void Aquarium::live()
{
	while (renderWindow.isOpen())
	{
		for (BaseEntity entity : inhabitants)
			if (entity.IsAlive())
				entity.live();
	}
}

void Aquarium::setSettings(int predatorsCount, int herbivorousCount, int planktonsCount)
{
	for (int count = 0; count < predatorsCount; count++) { PredatorFish predator; aquarium->inhabitants.push_back(predator); }
	for (int count = 0; count < herbivorousCount; count++) { HerbivoresFish herbivorous; aquarium->inhabitants.push_back(herbivorous); }
	for (int count = 0; count < planktonsCount; count++) { Plankton plankton; aquarium->inhabitants.push_back(plankton); }
}

void Aquarium::start()
{
	aquarium->live();
}