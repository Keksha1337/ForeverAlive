#pragma once
#include "BaseEntity.h"
class Plankton :
	public BaseEntity
{
private:
	virtual void proliferation();
public:
	Plankton();
};

