#pragma once
#include "SFML\Graphics.hpp"
#include <ctime>

class BaseEntity
{
protected:
	virtual void proliferation();
	virtual void move();
	void draw();
	void checkAlive();
	sf::Sprite sprite;
	sf::Texture texture;
	size_t birthdayTime;
	size_t leftSecForDie;
	int secForDie;
	int eatCounter;
	int eatForSec;
	int vision;
	bool isAlive;
public:
	bool IsAlive();
	void live();
	BaseEntity();
};

